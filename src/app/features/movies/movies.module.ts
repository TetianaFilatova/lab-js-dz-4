import { NgModule } from '@angular/core';
import { MoviesComponent } from './movies.component';
import { MoviesRoutingModule } from './movies-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MovieService } from 'src/app/core/services/movie/movie.service';

@NgModule({
  declarations: [
    MoviesComponent,
  ],
  imports: [
    MoviesRoutingModule,
    SharedModule,
  ],
  providers: [MovieService]
})
export class MoviesModule { }
